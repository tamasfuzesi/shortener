<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            CRISTO URL Shortener
        </title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
    </head>
    <body>
        <div id="app">
            <shortener></shortener>
        </div>
        <script>
        	window.csrfToken = '{{csrf_token()}}';
        </script>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
