require('./bootstrap');

Vue.component('shortener', require('./components/Shortener.vue'));

const app = new Vue({
    el: '#app'
});
