<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShortenerController extends Controller
{
    public function shortener(Request $request, $short = null){
    	if($request->ajax()){
    		if($this->isValidUrl($request->input('url'))){
    			return [
    				'status' => 'valid',
    				'url_code' => $this->generateUrl($request->input('url'))
    			];
    		}
    		else{
    			return ['status' => 'invalid'];
    		}
    	}
    	if($short){
    		$redirect = $this->getRedirect($short);
    		if($redirect){
    			return redirect()->away($redirect);
    		} else{
    			return redirect()->away(getenv('APP_URL'));
    		}
    	}
    	return view('index');
    }

    public function getRedirect($short_code){
    	$url = \App\Shortener::where('short_url', $short_code)->first();

    	if($url){
    		return $url->long_url;
    	}

    	return false;
    }

    public function isValidUrl($url){
	    if(!filter_var($url, FILTER_VALIDATE_URL) === false) {
	    	return true;
	    }

	    return false;
    }

    public function generateUrl($url){
    	while(true){
    		$short_url = \App\Shortener::where('long_url', $url)->first();
    		if($short_url){
    			return $short_url->short_url;
    		}

    		$short_url = substr(md5(time()), 0, 6);
    		if(! \App\Shortener::where('short_url', $short_url)->first()){
    			\App\Shortener::create(['long_url' => $url, 'short_url' => $short_url]);
    			return $short_url;
			}
    	}
    }
}
